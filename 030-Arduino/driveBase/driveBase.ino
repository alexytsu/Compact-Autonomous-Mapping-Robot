#include "Encoder.h"
#include "serialTests.h"
#include "globals.h"
// Mega has pins 2,3,18,19,20,21 available as interrupts
// Connect Yellow to 18, White to 19, Blue to GND, Green to 3.3V

Encoder encA(18,19);
Encoder encB(2,3);
Encoder encC(20,21);

// Globals
long oldA = 0;
long oldB = 0;
long oldC = 0;

long countA = 0;
long countB = 0;
long countC = 0;

float curSpeedA = 0;
float curSpeedB = 0;
float curSpeedC = 0;

float targetSpeedA = 0;
float targetSpeedB = 0;
float targetSpeedC = 0;

int targetSpeed = 0;
float curSpeed = 0;

long globX = 0;
long globY = 0;

int targetDirection = 0;
float curDirection = 0;

unsigned long prevMillis = 0;

bool stringComplete = false;
String inputString = "";

void setup() {
  Serial.begin(115200);
  Serial.println("Initialized!");
  inputString.reserve(20);
}

void parseInstruction(){
  if(stringComplete){
    char instruction[20];
    inputString.toCharArray(instruction, 20);
    sscanf(instruction, "%d,%d", &targetDirection, &targetSpeed);
    inputString = "";
    stringComplete = false;
  }
}

void loop(){

  parseInstruction();
  
  countA = encA.read();
  countB = encB.read();
  countC = encC.read();

  int rotate = 0;
  float targetSpeedX = cos(targetDirection*3.14159/180) * targetSpeed;
  float targetSpeedY = sin(targetDirection*3.14159/180) * targetSpeed;

  targetSpeedA = 0.5 * targetSpeedX - 0.866 * targetSpeedY + rotate;
  targetSpeedB = 0.5 * targetSpeedX + 0.866 * targetSpeedY + rotate;
  targetSpeedC = targetSpeedX + rotate;

  unsigned long currentMillis = millis();

  curSpeedA = abs(countA - oldA)*1000/(currentMillis - prevMillis);
  if(countA < oldA) curSpeedA *= -1;

  curSpeedB = abs(countB - oldB)*1000/(currentMillis - prevMillis);
  if(countB < oldB) curSpeedB *= -1;

  curSpeedC = abs(countC - oldC)*1000/(currentMillis - prevMillis);
  if(countC < oldC) curSpeedC *= -1;

  oldA = countA;
  oldB = countB;
  oldC = countC;

  delay(1000);
  prevMillis = currentMillis;

}

void serialEvent(){
  while(Serial.available()){
    char inChar = (char)Serial.read();
    inputString += inChar;
    if(inChar == '\n'){
      stringComplete = true;
    }
  }
}


